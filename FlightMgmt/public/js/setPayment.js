$(document).ready(function(){
	var cookie = window.cookie;
    $(window).scrollTop($('#select_page').offset().top);
    setPaymentInformation(cookie);
    $("#select1").load("/static/html/passengerinformation.html", function(responseTxt, statusTxt, xhr){
			if(statusTxt == "success")
				// var flights_json = JSON.stringify(flights[data])
				// window.cookie =num_adults+";"+flights_json;
				// alert("External content loaded successfully!");
			if(statusTxt == "error")
				alert("Error: " + xhr.status + ": " + xhr.statusText);
		});
    // $("#next_btn").click(function(){
    // 	next_step();
    // });
});

var flight;
var adults;

function setPaymentInformation(data){
	const TAX = 1000;
	var cookieArray = data.split(";");
	var tax = cookieArray[0] * TAX;
	adults = parseInt(cookieArray[0]);
	var detail = JSON.parse(cookieArray[1]);
	flight = create(detail.type,cookieArray[1]);
	var price = flight.getDetail().price*adults;
	var total = parseInt(price) + tax;
    $('#flight_payment').html("Flight : "+price+" บาท");
    $('#tax').html("Tax : "+tax+ " บาท");
    $('#total').html("Total : "+total+" บาท");
    $('#total').val(total);
}

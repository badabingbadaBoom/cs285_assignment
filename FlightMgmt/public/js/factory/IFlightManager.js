function IFlightManager(){}


IFlightManager.create = function(type,detail){
	var constr = type;
	var flight;
	IFlightManager[constr].prototype = new IFlightManager();
	flight = new IFlightManager[constr](detail);
	return flight;
}

function create(type,data){
	var flight;
	if(type == "one-way"){
		flight = IFlightManager.create("OneWay",data);
	}
	else if(type == "round-trip"){
		flight = IFlightManager.create("RoundTrip",data);
	}
	return flight;
}

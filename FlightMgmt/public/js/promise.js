var isMomHappy = true;

var willIGetNewPhone = new Promise(
	function(resolve, reject) {
		if (isMomHappy) {
			var phone = {
				brand: 'samsung',
				color: 'black'
			};
			resolve(phone);
		} else {
			var reason = new Error('mom is not happy');
			reject(reason);
		}
	}
);

// var searchFlight = new Promise(
// 	function (resolve , reject) {
// 	var flights = [
// 	{'price' : 100.5 , 'id' : 'SG123'},
// 	{'price' : 200 , 'id' : 'TG930'}]
// 	resolve(flights);
// });

// var askMom = function() {
// 	willIGetNewPhone.then(function(fullfiled) {
// 			console.log(fullfiled);
// 		})
// 		.catch(function(error) {
// 			console.log(error.message);
// 		});
// };

// var askMom = function () {
// 	searchFlight.then(function (fullfiled) {
// 		console.log(fullfiled);
// 	})
// 	.catch(function (error) {
// 		console.log(error.message);
// 	});
// };

var showOff = function(phone) {
	return new Promise(
		function(resolve, reject) {
			var message = 'Hey friend, I have a new ' + phone.color + ' ' + phone.brand + 'phone';
			resolve(message);
		}
	);
};

var askMom = function() {
	willIGetNewPhone.then(showOff)
		.then(function(fullfiled) {
			console.log(fullfiled);
		})
		.catch(function(error) {
			console.log(error.message);
		});
};

askMom();
$(document).ready(function(){
	__init();
	bindCheckBox();
	$('#reservation').click(function(){
		next();
	});
});

var extraInsurance;
var defaultValue = parseInt($('#total').val());
var totalPrice = 0;
var finalDiscount = 0;

function __init(){
	extraInsurance = new IExtraInsurance('IExtraInsurance');
}

function bindCheckBox(){
	$('#select-bag-insurance').change(function(){
		var ischecked= $(this).is(':checked');
    		if(!ischecked){
 				extraInsurance.remove('luggageInsurance');
    		}
    		else{
    			extraInsurance.decorate('luggageInsurance');
    		}
    		updateTotalExtraPrice();

	});	
	
	$('#select-travel-insurance').change(function(){
		var ischecked= $(this).is(':checked');
    		if(!ischecked){
    			extraInsurance.remove('travelInsurance');
    		}
    		else{
    			extraInsurance.decorate('travelInsurance');
    		}
    		updateTotalExtraPrice();
	});	
}

function updateTotalExtraPrice(){
	var total_package = extraInsurance.getPrice();
	$('#service').html("Services Fee : "+total_package+" บาท");
	updateExtraList();
	updateTotalPrice(total_package);
}

function updateExtraList(){
	var total_package = extraInsurance.getList();
	$('#serviceList').html(total_package);
}

function updateTotalPrice(ExtraPrice){
	var discountPrice = 0;
	if(extraInsurance.decorators_list.length >= 2){
		discountPrice = discount(ExtraPrice);
	}
	else{
		resetDiscount(discountPrice);
	}
	var total = (ExtraPrice + defaultValue) - discountPrice;
	totalPrice = total;
	$('#total').html("Total : "+total+" บาท");
}

function discount(ExtraPrice){
	var discount = (ExtraPrice*10)/100;
	$('#discount').html("Discount : -"+discount+" บาท");
	finalDiscount = discount;
	return discount;
}

function resetDiscount(discount){
	$('#discount').html("Discount : "+discount+" บาท");
}

function next(){
	$("#select1").load("/static/html/transaction_summary.html", function(responseTxt, statusTxt, xhr){
			if(statusTxt == "success")
				makePackage();
				// package.push(finalDiscount);
				// package.push(total);
				// var flights_json = JSON.stringify(flights[data])
				// window.cookie =num_adults+";"+flights_json;
				// alert("External content loaded successfully!");
			if(statusTxt == "error")
				alert("Error: " + xhr.status + ": " + xhr.statusText);
			});
}

function makePackage(){
	var total_package = extraInsurance.getList();
	if(total_package == ""){
		package.push("No ExtraPackage");
		package.push("No Discount");
	}
	else{
		package.push(total_package);
		package.push(finalDiscount);
	}
	if(totalPrice == 0)
		package.push(defaultValue);
	else
		package.push(totalPrice);
}
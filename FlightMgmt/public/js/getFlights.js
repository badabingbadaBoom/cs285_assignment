 // gobal varriable
var flights = [];
var type_trip;
var defaultPage = 1;
var defaultEntries = 3;
var select_status = false;
var num_adults;

// when document ready call getValue function to get input from HTML and set to getJSON function
$(document).ready(function(){
	try{
		setEntriesAction()
		var detail = getValue();
		type_trip = detail[0];
		getJSON(detail)
	}catch(err){
		if(err.message == 'fill_up')
		{
			var err_message = 'Please fill up this form.';
			setError(err_message); // send to manage error 
		}
	}
});

// showTable call showFlights to change table by set flighs(gobal_varriable) and page_number
var showTable = function(page,entries){
	//window.alert('PAGE : '+page+' Entries'+entries);
	showFlightsTable(flights,page,entries);
}

// Promise function to get json file
var getFlights = new Promise(function(resolve,reject){
	$.getJSON('/static/js/data.json', function(data,status){
		// alert('Status GET_JSON:'+status)
		if(status=='success')
		{
			resolve(data)
		}
		else
		{
			reject(status)
		}
	})
})

// function to generate table follow by page number
var showFlightsTable = function(flights,page,entries){
	var current = 0;
	var content;
	var card_content;
	var pagination_content="";
	var start = (page-1)*entries;
	var range = flights.slice(start,start+entries);
	for(var i=0;i < range.length ;i++)
	{
		// content+= '<tr><td class="font-card-price">'+range[i].price+'<span>&#3647;</span></td>'
		// +'<td class="font-card-detail">'+range[i].airline+'</td>'
		// +'<td class="font-card-detail">'+range[i].takeoff+'</td>'
		// +'<td class="font-card-detail">'+range[i].landing+'</td>'
		// +'<td class="font-card-detail">'+'<a href="#">detail</a> '
		// +range[i].stop+' <i class="fa fa-print"></i></td>'
		// +'</tr>';

		card_content+='<tr><td><div class="card-group">'
		+'<div class="card bg-light font-card-price"><div class="card-body">'+range[i].price+'<span>&#3647;</span></div></div>'
		+'<div class="card bg-light font-card-detail"><div class="card-body text-center">'+range[i].airline+'</div></div>'
		+'<div class="card bg-light font-card-detail"><div class="card-body text-center">'+range[i].takeoff+'</div></div>'
		+'<div class="card bg-light font-card-detail"><div class="card-body text-center">'+range[i].landing+'</div></div>'
		+'<div class="card bg-light font-card-detail"><div class="card-body text-center">'
		+'<a href="javascript: toggleFooter('+i+')">detail</a> '
		+range[i].stop+' <i class="fa fa-print"></i></div></div>'
		+'</div><div class="card-footer font-card-detail" id="'+i+'">'
		+'<div class="row">'
		+'<div class="col-sm-6">'
		+'<div class="form-group">'
		+'<div class="col-sm-3">'+range[i].outboundName+'</div>'
		+'<div class="col-sm">'+range[i].outboundDate+'</div>'
		+'<div class="col-sm">'+range[i].takeoff+' '+range[i].departing+'</div>'
		+'<div class="col-sm">'+range[i].landing+' '+range[i].destination+'</div>'
		+'<div class="col-sm">Duration: '+range[i].stop+'</div>'
		// +'</div>'
		// +'</div>'
		// +'</div>'
		// +'<div><button type="button" class="btn btn-primary font-submit" id="select">Select</button></div>'
		// +'</div></td></tr>';
		if(type_trip == "round-trip"){
		card_content+='<div class="col-sm-3">'+range[i].inboundName+'</div>'
		+'<div class="col-sm">'+range[i].inboundDate+'</div>'
		+'<div class="col-sm">'+range[i].in_takeoff+' '+range[i].in_departing+'</div>'
		+'<div class="col-sm">'+range[i].in_landing+' '+range[i].in_destination+'</div>'
		+'<div class="col-sm">Duration: '+range[i].in_stop+'</div>'
		}
		card_content+='</div>'
		+'</div>'
		+'</div>'
		+'<div><button type="button" class="btn btn-primary font-submit" id="select" onClick="select(\'' + (i+start) + '\')">Select</button></div>'
		+'</div></td></tr>';

		current++;
	}
	$(".flights-body-card").html(card_content);
	// $(".flights-body").html(content);
	$(".list-data-header").html('<span class="list-shown">'+(start+current)+'</span> of '+flights.length+' shown - '+type_trip);
	var next_page;
	var previous_page;
	if(page <= 1)
	{
		pagination_content+='<li class="page-item disabled">';
	}
	else
	{
		pagination_content+='<li class="page-item">';
	}
	pagination_content+='<a class="page-link" href="javascript: onClick=showTable('+(page-1)+','+entries+')" aria-label="previous">'+
	'<span aria-hidden="true">&laquo;</span>'+
	'<span class="sr-only">Previous</span>'+		
	'</a>'+
	'</li>';
	for(var i=0; i < Math.ceil(flights.length/entries) ;i++){
		var number = i+1;
		if(number == page)
		{
			pagination_content+='<li class="page-item active"><a class="page-link" href="javascript: onClick=showTable('+number+','+entries+')">'+number+'</a></li>'
		}
		else
		{
			pagination_content+='<li class="page-item"><a class="page-link" href="javascript: onClick=showTable('+number+','+entries+')">'+number+'</a></li>'
		}
	}
	if(page >= Math.ceil(flights.length/entries))
	{
		pagination_content+='<li class="page-item disabled">';
	}
	else
	{
		pagination_content+='<li class="page-item">';
	}
	pagination_content+='<a class="page-link" href="javascript: onClick=showTable('+(page+1)+','+entries+')" aria-label="Next">'+
	'<span aria-hidden="true">&raquo;</span>'+
	'<span class="sr-only">Next</span>'+
	'</a>'+
	'</li>';
	$(".pagination-number").html(pagination_content)
	hideFooter();
}

// function to get value in page html by JQuery and return to array
var getValue = function(){
	var type = $('input[name=type]:checked').val();
	var fly_from = $('#fly-from').val();
	var fly_to = $('#fly-to').val();
	var departing = $('#departing-airport').val();
	var destination = $('#destination-airport').val();
	var adults = $('#adults').val();
	var travel_class = $('#travel-class').val();
	// window.alert('FlightType:'+type+'  Flying_from:'+fly_from+'  Flying_to:'+fly_to+'  Desparting:'+departing+
	// 	'  Destination:'+destination+'  Adults:'+adults+'  Travel-Class:'+travel_class);
	num_adults = adults;
	// Cookies.set("adults",adults);
	var detail_data = [type,fly_from,fly_to,departing,destination,adults,travel_class];
	if(departing == '' || destination == '' || adults == '')
	{
		throw new Error('fill_up')
	}
	else
	{
		return detail_data;
	}
}

// function select * form JSON file where detail in html = detail in JSON data
var getJSON = function(detail){
	getFlights.then(function(fullfiled){
		for(var i=0;i < fullfiled.length;i++)
		{
			var departing = fullfiled[i].takeoff.split(" ");
			var destination = fullfiled[i].landing.split(" ");
			var type = fullfiled[i].type.split(" ");
			if(departing[0] == detail[3] && destination[0] == detail[4] && type[0] == detail[0])
			{
				flights.push(fullfiled[i])
			}
		}
		showFlightsTable(flights,defaultPage,defaultEntries)
	}).catch(function(err){
		window.alert(err.message)
	})
}

// function set error warning when user don't fill up information
function setError(err){
	$('#warning').attr({"class" : "alert alert-warning alert-dismissible"})
	var content = '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
	'<strong>Warning!</strong> '+err+'';
	$('#warning').html(content);
}

function setEntriesAction(){
	$('#entries').change(function(){
		var entries = $('#entries').val()
		showFlightsTable(flights,defaultPage,entries)
	})
}

function toggleFooter(IDofObject){
	$("#"+IDofObject).toggle();
}

function hideFooter(){
	$(".card-footer").hide();
}

function select(data){
	// alert(flights[data].takeoff);
	$("#select_page").load("/static/html/information.html", function(responseTxt, statusTxt, xhr){
			if(statusTxt == "success")
				var flights_json = JSON.stringify(flights[data])
				window.cookie =num_adults+";"+flights_json;
				// alert("External content loaded successfully!");
			if(statusTxt == "error")
				alert("Error: " + xhr.status + ": " + xhr.statusText);
		});
}




function IExtraInsurance() {
    this.name = "Extra Insurance";
    console.log("Initializing " + this.name);
    this.decorators_list = []; 
}

IExtraInsurance.decorators = {};

IExtraInsurance.decorators.travelInsurance = {
    name: "Travel Insurance",
    price: "1300",
    init: function () {
        console.log("added Travel Insurance " + this.name);
        return this.name;
    },
    getPrice : function(){
        return parseInt(this.price);
    },
    getList : function(){
        var text = "-"+this.name+" "+this.price+" บาท";
        return text;
    }
};
IExtraInsurance.decorators.luggageInsurance = {
    name: "Luggage Insurance",
    price: "350",
    init: function () {
        console.log("added Luggage Insurance " + this.name);
        return this.name;
    },
    getPrice : function(){
        return parseInt(this.price);
    },
    getList : function(){
        var text = "-"+this.name+" "+this.price+" บาท";
        return text;
    }
};

IExtraInsurance.prototype.decorate = function (decorator) {
    this.decorators_list.push(decorator);
};

IExtraInsurance.prototype.remove = function(decorator){
    var index = this.decorators_list.indexOf(decorator);
    if(index > -1){
        this.decorators_list.splice(index, 1);
    }
};

IExtraInsurance.prototype.init = function () {
    all_insurance = "";
    for (i = 0; i < this.decorators_list.length; i += 1) {
        decorator_name = this.decorators_list[i];
        all_insurance += IExtraInsurance.decorators[decorator_name].init()+" ";
    }
    return all_insurance;
};

IExtraInsurance.prototype.getPrice = function(){
    var total = 0;
    for (var i = 0; i < this.decorators_list.length; i++) {
        decorator_name = this.decorators_list[i];
        total += IExtraInsurance.decorators[decorator_name].getPrice();
    }
    return total;
}

IExtraInsurance.prototype.getList = function(){
    var list = "";
    for (var i = 0; i < this.decorators_list.length; i++) {
        decorator_name = this.decorators_list[i];
        list += IExtraInsurance.decorators[decorator_name].getList()+"<br>";
    }
    return list;
}
